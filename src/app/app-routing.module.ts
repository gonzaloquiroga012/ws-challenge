import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesListComponent } from './components/movies-list/movies-list.component';
import { MasterComponent } from './components/master/master.component';

const routes: Routes =
  [
    { path: '', redirectTo: '/page/1', pathMatch: 'full' },
    {
      path: 'page/:page',
      component: MasterComponent,
      children: [
        {
          path: 'character/:id/movies',
          component: MoviesListComponent
        }
      ]
    }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
