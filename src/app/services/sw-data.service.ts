import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient, HttpParams } from '@angular/common/http';
import { People } from "../model/people";
import { character } from '../model/character';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SwDataService {
  private baseUrl: String = environment.baseUrl;
  private peopleUrl: string = environment.peopleUrl;
  constructor(
    public http: HttpClient,
    public activedRoute: ActivatedRoute
  ) { }

  public getCharacters(page?) {

    return new Promise((resolve, reject) => {
      if (!page) {
        page = 1;
      }
      let params = new HttpParams().set('page', page);
      this.http.get<People>(this.baseUrl + this.peopleUrl, { params: params })
        .subscribe(
          (result: People) => {
            resolve(result);
          },
          (error) => {
            reject(error);
          })
    });
  }

  public getCharacter(id) {
    return new Promise((resolve, reject) => {
      let path = '/' + id;
      this.http.get<character>(this.baseUrl + this.peopleUrl + path)
        .subscribe(
          (result: character) => {
            resolve(result);
          },
          (error) => {
            reject(error);
          })
    });
  }

  public getMovies(url) {
    return new Promise((resolve, reject) => {
      this.http.get<any>(url)
        .subscribe(
          (result: character) => {
            resolve(result);
          },
          (error) => {
            reject(error);
          })
    });
  }

}
