import { character } from "./character";

export interface People{
    count: number;
    next: string;
    previous: string;
    results: character[];
    other: string;
}


