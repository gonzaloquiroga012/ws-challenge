import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SwDataService } from 'src/app/services/sw-data.service';
import { character } from 'src/app/model/character';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {
  private id: number;
  public character: character;
  public movies: any[];
  public loading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private swDataSrv: SwDataService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      if (this.id) {
        this.loadData();
      }
    })
  }

  private loadData() {
    this.loading = true;
    this.swDataSrv.getCharacter(this.id)
      .then((res: character) => {
        this.character = res;
        this.loadMovies();
      })
      .catch((err) => {
        alert('An error occurred. please try later');
      })
  }

  private loadMovies() {
    this.movies = [];
    let moviesPromises = [];
    this.character.films.forEach(element => {
      moviesPromises.push(this.swDataSrv.getMovies(element));
    });

    Promise.all(moviesPromises)
      .then((res) => {
        this.movies = (res);
      })
      .catch((err) => {
        alert('An error occurred. please try later');
      })
      .finally(() => {
        this.loading = false;
      })
  }

}
