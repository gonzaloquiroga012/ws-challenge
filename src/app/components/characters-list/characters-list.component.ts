import { Component, OnInit } from '@angular/core';
import { SwDataService } from "../../services/sw-data.service";
import { People } from 'src/app/model/people';
import { Router, ActivatedRoute } from '@angular/router';
import { character } from 'src/app/model/character';
@Component({
  selector: 'app-characters-list',
  templateUrl: './characters-list.component.html',
  styleUrls: ['./characters-list.component.css']
})
export class CharactersListComponent implements OnInit {

  public people: People;
  public loading: boolean = false;
  private page: string;
  constructor(
    private SwDataSrv: SwDataService,
    public route: Router,
    public activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(() => {
      this.getData();
    })
  }

  private getData() {
    this.loading = true;
    this.SwDataSrv.getCharacters(this.activeRoute.snapshot.paramMap.get("page"))
      .then((res: People) => {
        this.people = res;
        this.loading = false;
      }).catch(err => {
        alert('An error occurred. please try later');
        console.log(err);
      });
  }

  public selectCharacter(character: character) {

    let id = this.getIndex(character.url);
    this.route.navigate(['page', this.activeRoute.snapshot.paramMap.get("page"), 'character', id, 'movies']);

  }

  private getIndex(url) {
    return url.replace('https://swapi.co/api/people/', '').replace('/', '');//TODO change
  }

  public onSelected($event) {
    this.loading = !$event.isLoaded;
    this.page = $event.value;
  }
}
