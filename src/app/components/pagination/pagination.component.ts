import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { People } from 'src/app/model/people';
import { SwDataService } from 'src/app/services/sw-data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  @Input() people: People;
  @Input() elements: number;
  @Output() selected = new EventEmitter<{ isLoaded: boolean }>();

  public pages: Array<{ label: string, number: number }> = [];
  public pageSelected: { label: string, number: number };
  constructor(
    private SwDataSrv: SwDataService,
    private route: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.initPages();
  }

  public initPages() {
    let numberOfPages = Math.ceil(this.people.count / this.elements);
    for (let index = 1; index <= numberOfPages; index++) {
      this.pages.push({ label: index.toString(), number: index })
    }
    let num = +this.activatedRoute.snapshot.paramMap.get('page');
    this.pageSelected = { label: num.toString(), number: num };
  }

  public goToPage(page) {
    this.pageSelected = page;
    this.loadData(page.number);
  }

  public goToPrevious() {
    let previous = this.pageSelected.number - 1;
    if (previous > 0) {
      this.loadData(previous);
      this.pageSelected = this.pages[previous - 1];
    }
  }

  public goToNext() {
    let next = this.pageSelected.number + 1;
    if (next < this.elements) {
      this.loadData(next);
      this.pageSelected = this.pages[next - 1];
    }
  }

  private loadData(numberPage) {
    this.route.navigate(['page', numberPage]);
  }
}
