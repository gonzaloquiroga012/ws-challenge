import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { CharactersListComponent } from './components/characters-list/characters-list.component';
import { MoviesListComponent } from './components/movies-list/movies-list.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { MasterComponent } from './components/master/master.component';
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    CharactersListComponent,
    MoviesListComponent,
    PaginationComponent,
    MasterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
